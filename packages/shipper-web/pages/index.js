import React from "react";
import { Button as B } from "@saloodo/ui-components";

const propTypes = {};
const defaultProps = {};

const Shipper = props => (
  <>
    <h1>Shipper</h1>
    <B />
  </>
);

Shipper.propTypes = propTypes;
Shipper.defaultProps = defaultProps;

export default Shipper;
