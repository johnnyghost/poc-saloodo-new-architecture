# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.6](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/shipper-web@0.1.5...@saloodo/shipper-web@0.1.6) (2019-01-16)

**Note:** Version bump only for package @saloodo/shipper-web





## [0.1.5](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/shipper-web@0.1.4...@saloodo/shipper-web@0.1.5) (2019-01-10)

**Note:** Version bump only for package @saloodo/shipper-web





## [0.1.4](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/shipper-web@0.1.3...@saloodo/shipper-web@0.1.4) (2019-01-10)


### Bug Fixes

* test ([d05fc82](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/d05fc82))





## [0.1.3](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/shipper-web@0.1.2...@saloodo/shipper-web@0.1.3) (2019-01-10)

**Note:** Version bump only for package @saloodo/shipper-web





## [0.1.2](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/shipper-web@0.1.1...@saloodo/shipper-web@0.1.2) (2019-01-09)

**Note:** Version bump only for package @saloodo/shipper-web





## [0.1.1](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/shipper-web@0.1.0...@saloodo/shipper-web@0.1.1) (2019-01-09)


### Bug Fixes

* index page. ([f5eb1a0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/f5eb1a0))





# [0.1.0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/shipper-web@0.0.5-rc.2...@saloodo/shipper-web@0.1.0) (2019-01-09)


### Features

* some tests. ([02f0661](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/02f0661))





## [0.0.5-rc.2](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/shipper-web@0.0.5-rc.1...@saloodo/shipper-web@0.0.5-rc.2) (2019-01-09)


### Bug Fixes

* some tests. ([dd0fce7](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/dd0fce7))





## [0.0.5-rc.1](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/shipper-web@0.0.5-rc.0...@saloodo/shipper-web@0.0.5-rc.1) (2019-01-09)


### Bug Fixes

* some tests. ([f1b00a4](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/f1b00a4))





## 0.0.5-rc.0 (2019-01-09)


### Bug Fixes

* some tests. ([a2d5d77](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/a2d5d77))



## 0.0.4 (2019-01-09)



## 0.0.3 (2019-01-09)



## 0.0.2 (2019-01-09)


### Features

* shipper-web. ([4e1e69b](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/4e1e69b))



## 0.0.1 (2019-01-09)
