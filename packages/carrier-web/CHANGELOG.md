# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.8.1](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.8.0...@saloodo/carrier-web@0.8.1) (2019-01-16)


### Bug Fixes

* test. ([0d7c0d9](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/0d7c0d9))





# [0.8.0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.7.0...@saloodo/carrier-web@0.8.0) (2019-01-16)


### Features

* test ([698b7b4](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/698b7b4))





# [0.7.0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.6.0...@saloodo/carrier-web@0.7.0) (2019-01-10)


### Features

* test. ([4f5ff17](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/4f5ff17))





# [0.6.0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.5.1...@saloodo/carrier-web@0.6.0) (2019-01-10)


### Features

* test. ([a9d7709](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/a9d7709))





## [0.5.1](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.5.0...@saloodo/carrier-web@0.5.1) (2019-01-10)


### Bug Fixes

* test ([d05fc82](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/d05fc82))





# [0.5.0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.4.0...@saloodo/carrier-web@0.5.0) (2019-01-10)


### Features

* test. ([3c9a915](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/3c9a915))





# [0.4.0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.3.0...@saloodo/carrier-web@0.4.0) (2019-01-10)


### Features

* test. ([e15af4c](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/e15af4c))





# [0.3.0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.2.3...@saloodo/carrier-web@0.3.0) (2019-01-10)


### Features

* test. ([37ca63b](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/37ca63b))





## [0.2.3](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.2.2...@saloodo/carrier-web@0.2.3) (2019-01-09)

**Note:** Version bump only for package @saloodo/carrier-web





## [0.2.2](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.2.1...@saloodo/carrier-web@0.2.2) (2019-01-09)


### Bug Fixes

* index page. ([f5eb1a0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/f5eb1a0))





## [0.2.1](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.2.0...@saloodo/carrier-web@0.2.1) (2019-01-09)


### Bug Fixes

* update. ([107aee8](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/107aee8))





# [0.2.0](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/compare/@saloodo/carrier-web@0.1.0...@saloodo/carrier-web@0.2.0) (2019-01-09)


### Features

* update. ([342a29b](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/342a29b))





# 0.1.0 (2019-01-09)


### Bug Fixes

* update. ([c835da7](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/c835da7))



## 0.0.4 (2019-01-09)



## 0.0.3 (2019-01-09)



## 0.0.2 (2019-01-09)


### Features

* shipper-web. ([4e1e69b](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/4e1e69b))



## 0.0.1 (2019-01-09)


### Bug Fixes

* remove dist folders. ([8e202be](https://gitlab.com/johnnyghost/poc-saloodo-new-architecture/commit/8e202be))
