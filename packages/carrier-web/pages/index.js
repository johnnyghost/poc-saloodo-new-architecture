import React from "react";
import { Button as B } from "@saloodo/ui-components";

const propTypes = {};
const defaultProps = {};

const Carrier = props => (
  <>
    <h1>Carrier</h1>
    <B />
  </>
);

Carrier.propTypes = propTypes;
Carrier.defaultProps = defaultProps;

export default Carrier;
