import React from "react";

const propTypes = {};
const defaultProps = {};

/**
 * <Button />
 * @param {*} props
 */
const Button = props => <button>button 2</button>;

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;
